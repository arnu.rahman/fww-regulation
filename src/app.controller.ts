import {
  Body,
  Controller,
  HttpCode,
  Logger,
  Post,
  UseGuards,
  ForbiddenException
} from '@nestjs/common';
import { AppService } from './app.service';
import { IdentityNumberDto } from './dto/identity-number.dto';
import { CivilDto } from './dto/civil.dto';
import { AuthGuard } from '@nestjs/passport';
import { ApiBasicAuth, ApiForbiddenResponse, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { InternalServerErrorResponseDto } from './dto/internal-server-error.response.dto';
import { UnauthorizedResponseDto } from './dto/unauthorized.response.dto';

@ApiTags('Regulation')
@ApiForbiddenResponse({ type: InternalServerErrorResponseDto })
@ApiUnauthorizedResponse({ type: UnauthorizedResponseDto })
@ApiBasicAuth()
@Controller({ version: '1' })
export class AppController {
  private readonly logger = new Logger(AppController.name);
  constructor(private readonly appService: AppService) { }

  @UseGuards(AuthGuard('basic'))
  @HttpCode(200)
  @Post('check-imigrasi')
  async checkImigrasi(@Body() identityNumberDto: IdentityNumberDto) {
    this.logger.log('[POST] /api/v1/check-imigrasi');
    try {
      this.logger.log(
        `Return result of checking imigration`,
      );
      return await this.appService.checkImigrasi(identityNumberDto);
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @HttpCode(200)
  @Post('check-peduli-lindungi')
  async checkPeduliLindungi(@Body() identityNumberDto: IdentityNumberDto) {
    this.logger.log('[POST] /api/v1/check-peduli-lindungi');
    try {
      this.logger.log(
        `Return result of checking peduli lindungi`,
      );
      return await this.appService.checkPeduliLindungi(identityNumberDto);
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @HttpCode(200)
  @Post('check-disdukcapil')
  async checkDisdukcapil(@Body() civilDto: CivilDto) {
    this.logger.log('[POST] /api/v1/check-disdukcapil');
    try {
      this.logger.log(
        `Return result of checking disdukcapil`,
      );
      return await this.appService.checkDisDukCapil(civilDto);
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Forbidden');
    }
  }
}

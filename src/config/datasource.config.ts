import { DataSource, DataSourceOptions } from "typeorm";
import * as dotenv from 'dotenv'
import { BlackListImigration } from "../entity/blacklist-imigration.entity";
import { UnBoosterPeduliLindungi } from "../entity/unbooster-pedulilindungi.entity";
import { CovidPeduliLindungi } from "../entity/covid-pedulilindungi.entity";
import { DataDisDukCapil } from "src/entity/data-disdukcapil.entity";
dotenv.config();

export const dataSourceOptions: DataSourceOptions = {
    type: 'mariadb',
    host: process.env.MYSQL_HOST,
    port: parseInt(process.env.MYSQL_PORT),
    username: process.env.MYSQL_USERNAME,
    password: process.env.MYSQL_PASSWORD,
    database:  process.env.MYSQL_DATABASE,
    entities: [BlackListImigration, DataDisDukCapil, UnBoosterPeduliLindungi, CovidPeduliLindungi],
    synchronize: false,
    logging: false,
    dateStrings: true
}

const dataSource = new DataSource(dataSourceOptions);
dataSource.initialize();
export default dataSource;

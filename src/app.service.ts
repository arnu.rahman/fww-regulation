import { Injectable, Logger } from '@nestjs/common';
import { IdentityNumberDto } from './dto/identity-number.dto';
import { ResponseImigrasi } from './interface/response-imigrasi.interface';
import { UnBoosterPeduliLindungiRepository } from './repository/unbooster-pedulilindungi.repository';
import { ResponsePeduliLindungi } from './interface/response-pedulilindungi.interface';
import { CivilDto } from './dto/civil.dto';
import { ResponseDisdukcapil } from './interface/response-disdukcapil.interface';
import { CovidPeduliLindungiRepository } from './repository/covid-pedulilindungi.repository';
import { BlackListImigrationRepository } from './repository/blacklist-imigration.repository';
import { DataDisDukCapilRepository } from './repository/data-disdukcapil.repository';

@Injectable()
export class AppService {
  private readonly logger = new Logger(AppService.name);
  constructor(
    private readonly blackListImigrationRepository: BlackListImigrationRepository,
    private readonly unboosterPeduliLindungi: UnBoosterPeduliLindungiRepository,
    private readonly disDukCapilRepository: DataDisDukCapilRepository,
    private readonly covidRepository: CovidPeduliLindungiRepository,
  ) {}

  async checkImigrasi(
    identityNumberDto: IdentityNumberDto,
  ): Promise<ResponseImigrasi> {
    const dataFound =
      await this.blackListImigrationRepository.findOneByIdentityNumber(
        identityNumberDto,
      );

    if (dataFound) {
      this.logger.log('Result checking migrasi prohibited to fly');
      return {
        identityNumber: identityNumberDto.identityNumber,
        allowedFly: false,
        reason: dataFound.reason,
      } as ResponseImigrasi;
    }

    this.logger.log('Result checking imigrasi allowed to fly');
    return {
      identityNumber: identityNumberDto.identityNumber,
      allowedFly: true,
      reason: null,
    } as ResponseImigrasi;
  }

  async checkPeduliLindungi(
    identityNumberDto: IdentityNumberDto,
  ): Promise<ResponsePeduliLindungi> {
    const dataFoundCovid =
      await this.covidRepository.findOneByIdentityNumber(
        identityNumberDto,
      );

    if (dataFoundCovid) {
      this.logger.log('Result checking peduli lindungi prohibited to fly because covid detected');
      return {
        identityNumber: identityNumberDto.identityNumber,
        isCovid: true,
        vaksinCovid: 0,
        shouldPCR: true,
      } as ResponsePeduliLindungi;
    }

    const dataFound =
      await this.unboosterPeduliLindungi.findOneByIdentityNumber(
        identityNumberDto,
      );

    if (dataFound) {
      this.logger.log('Result checking peduli lindungi should attach PCR');
      return {
        identityNumber: identityNumberDto.identityNumber,
        isCovid: false,
        vaksinCovid: dataFound.vaksin,
        shouldPCR: true,
      } as ResponsePeduliLindungi;
    }

    this.logger.log('Result checking pedulilindungi can to fly');
    return {
      identityNumber: identityNumberDto.identityNumber,
      isCovid: false,
      vaksinCovid: 3,
      shouldPCR: false,
    } as ResponsePeduliLindungi;
  }

  async checkDisDukCapil(civilDto: CivilDto): Promise<ResponseDisdukcapil> {
    const dataFound =
      await this.disDukCapilRepository.findOneByIdentityNumber(
        civilDto.identityNumber,
      );

    if (dataFound) {
      if (
        civilDto.name.toUpperCase().trim() ===
          dataFound.name.toUpperCase().trim() &&
        civilDto.birthDate === String(dataFound.birthDate)
      ) {
        this.logger.log('Result checking disdukcapil real match');
        return {
          identityNumber: civilDto.identityNumber,
          existData: true,
        } as ResponseDisdukcapil;
      } else {
        this.logger.log('Result checking disdukcapil unmatch');
        return {
          identityNumber: civilDto.identityNumber,
          existData: false,
        } as ResponseDisdukcapil;
      }
    }

    this.logger.log('Result checking disdukcapil default match');
    return {
      identityNumber: civilDto.identityNumber,
      existData: false,
    } as ResponseDisdukcapil;
  }
}

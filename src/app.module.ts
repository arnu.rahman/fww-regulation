import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { LoggerModule } from 'nestjs-pino';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UnBoosterPeduliLindungiRepository } from './repository/unbooster-pedulilindungi.repository';
import { AuthModule } from './auth/auth.module';
import { dataSourceOptions } from './config/datasource.config';
import { CovidPeduliLindungiRepository } from './repository/covid-pedulilindungi.repository';
import { DataDisDukCapilRepository } from './repository/data-disdukcapil.repository';
import { BlackListImigrationRepository } from './repository/blacklist-imigration.repository';

@Module({
  imports: [
    ConfigModule.forRoot(),
    LoggerModule.forRoot({
      pinoHttp: {
        formatters: {
          level: (label) => {
            return { level: label.toUpperCase() };
          },
        },
        customLevels: {
          emergerncy: 80,
          alert: 70,
          critical: 60,
          error: 50,
          warn: 40,
          notice: 30,
          info: 20,
          debug: 10,
        },
        useOnlyCustomLevels: true,
        transport: {
          target: 'pino-pretty',
          options: {
            singleLine: true,
            colorize: true,
            levelFirst: true,
            translateTime: 'SYS:standard',
            ignore: 'hostname,pid',
          },
        },
      },
    }),
    TypeOrmModule.forRoot(dataSourceOptions),
    AuthModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    BlackListImigrationRepository,
    UnBoosterPeduliLindungiRepository,
    DataDisDukCapilRepository,
    CovidPeduliLindungiRepository
  ],
})
export class AppModule {}

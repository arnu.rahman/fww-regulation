import { PickType } from '@nestjs/mapped-types';
import { CivilDto } from './civil.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, Length } from 'class-validator';

export class IdentityNumberDto extends PickType(CivilDto, ['identityNumber']) {
  @ApiProperty()
  @IsString()
  @Length(16)
  @IsNotEmpty()
  identityNumber: string;
}

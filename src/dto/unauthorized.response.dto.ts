import { ApiProperty } from '@nestjs/swagger';
import { ResponseBaseDto } from './response-base.dto';

export class UnauthorizedResponseDto extends ResponseBaseDto{
  @ApiProperty({ example: 401 })
  statusCode: number;

  @ApiProperty({ example: 'This is sample message unauthorized' })
  message: string;

  @ApiProperty({ example: 'Unauthorized' })
  error: string;
}

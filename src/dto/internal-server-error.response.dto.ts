import { ApiProperty } from '@nestjs/swagger';
import { ResponseBaseDto } from './response-base.dto';

export class InternalServerErrorResponseDto extends ResponseBaseDto{
  @ApiProperty({ example: 500 })
  statusCode: number;

  @ApiProperty({ example: 'This is sample message internal server error' })
  message: string;

  @ApiProperty({ example: 'Forbidden' })
  error: string;
}

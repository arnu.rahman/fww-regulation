import { Injectable, Logger } from '@nestjs/common';
import { IdentityNumberDto } from '../dto/identity-number.dto';
import { CovidPeduliLindungi } from '../entity/covid-pedulilindungi.entity';
import { DataSource, Repository } from 'typeorm';

@Injectable()
export class CovidPeduliLindungiRepository extends Repository<CovidPeduliLindungi> {
  private readonly logger = new Logger(CovidPeduliLindungiRepository.name);
  constructor(dataSource: DataSource) {
    super(CovidPeduliLindungi, dataSource.createEntityManager());
  }

  async findOneByIdentityNumber(
    identityNumberDto: IdentityNumberDto,
  ): Promise<CovidPeduliLindungi> {
    this.logger.log('Get data from DB');
    return await this.findOneBy(identityNumberDto);
  }
}

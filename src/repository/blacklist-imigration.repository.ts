import { Injectable, Logger } from '@nestjs/common';
import { IdentityNumberDto } from '../dto/identity-number.dto';
import { DataSource, Repository } from 'typeorm';
import { BlackListImigration } from '../entity/blacklist-imigration.entity';

@Injectable()
export class BlackListImigrationRepository extends Repository<BlackListImigration> {
  private readonly logger = new Logger(BlackListImigrationRepository.name);
  constructor(dataSource: DataSource) {
    super(BlackListImigration, dataSource.createEntityManager());
  }

  async findOneByIdentityNumber(
    identityNumberDto: IdentityNumberDto,
  ): Promise<BlackListImigration> {
    this.logger.log('Get data from DB');
    return await this.findOneBy(identityNumberDto);
  }
}

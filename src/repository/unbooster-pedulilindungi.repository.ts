import { Injectable, Logger } from '@nestjs/common';
import { IdentityNumberDto } from 'src/dto/identity-number.dto';
import { UnBoosterPeduliLindungi } from 'src/entity/unbooster-pedulilindungi.entity';
import { DataSource, Repository } from 'typeorm';

@Injectable()
export class UnBoosterPeduliLindungiRepository extends Repository<UnBoosterPeduliLindungi> {
  private readonly logger = new Logger(UnBoosterPeduliLindungiRepository.name);
  constructor(dataSource: DataSource) {
    super(UnBoosterPeduliLindungi, dataSource.createEntityManager());
  }

  async findOneByIdentityNumber(
    identityNumberDto: IdentityNumberDto,
  ): Promise<UnBoosterPeduliLindungi> {
    this.logger.log('Get data from DB');
    return await this.findOneBy(identityNumberDto);
  }
}

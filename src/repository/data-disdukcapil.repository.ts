import { Injectable, Logger } from '@nestjs/common';
import { DataDisDukCapil } from '../entity/data-disdukcapil.entity';
import { DataSource, Repository } from 'typeorm';

@Injectable()
export class DataDisDukCapilRepository extends Repository<DataDisDukCapil> {
  private readonly logger = new Logger(DataDisDukCapilRepository.name);
  constructor(dataSource: DataSource) {
    super(DataDisDukCapil, dataSource.createEntityManager());
  }

  async findOneByIdentityNumber(
    identityNumber: string,
  ): Promise<DataDisDukCapil> {
    this.logger.log('Get data from DB');
    return await this.findOneBy({ identityNumber });
  }
}

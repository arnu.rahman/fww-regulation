export interface ResponsePeduliLindungi {
  identityNumber: string;
  isCovid: boolean;
  vaksinCovid: number;
  shouldPCR: boolean;
}

export interface ResponseImigrasi {
  identityNumber: string;
  allowedFly: boolean;
  reason: string | null;
}

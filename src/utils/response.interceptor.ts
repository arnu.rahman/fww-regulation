import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { firstValueFrom, of } from 'rxjs';

@Injectable()
export class ResponseInterceptor implements NestInterceptor {
  public constructor(private readonly reflector: Reflector) {}

  public async intercept(
    context: ExecutionContext,
    next: CallHandler<any>,
  ): Promise<any> {
    const defaultMessageResponse = {
      200: 'OK',
      201: 'Created',
      202: 'Accepted',
      203: 'NonAuthoritativeInfo',
      204: 'NoContent',
      205: 'ResetContent',
      206: 'PartialContent',
    };

    const body = await firstValueFrom(next.handle());
    if (body === undefined) {
      return of({
        message: null,
      });
    }

    const request = context.switchToHttp().getRequest<Request>();
    const status =
      this.reflector.get<number>('__httpCode__', context.getHandler()) ||
      (request.method === 'POST' ? 201 : 200);

    let messageResponse: string = null;
    if (defaultMessageResponse[status] !== undefined) {
      messageResponse = defaultMessageResponse[status];
    }

    if (body.messageResponse !== undefined) {
      messageResponse = body.messageResponse;
      delete body.messageResponse;
    }

    return of({
      statusCode: status,
      message: messageResponse,
      data: body.data !== undefined ? body.data : body,
    });
  }
}

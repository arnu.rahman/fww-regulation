import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('covid_pedulilindungi')
export class CovidPeduliLindungi {
  @PrimaryColumn({ name: 'identity_number', length: 16 })
  identityNumber: string;

  @Column({ name: 'is_covid', type: 'boolean' })
  isCovid: number;
}

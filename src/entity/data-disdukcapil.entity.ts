import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('data_disdukcapil')
export class DataDisDukCapil {
  @PrimaryColumn({ name: 'identity_number', length: 16 })
  identityNumber: string;

  @Column({ length: 50 })
  name: string;

  @Column({ name: 'birth_date', type: 'date' })
  birthDate: Date;
}

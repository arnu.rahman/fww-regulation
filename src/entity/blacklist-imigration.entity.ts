import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('blacklist_imigration')
export class BlackListImigration {
  @PrimaryColumn({ name: 'identity_number', length: 16 })
  identityNumber: string;

  @Column({ length: 50 })
  reason: string;
}

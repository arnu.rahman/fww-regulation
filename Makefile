SERVICE_NAME := fww-regulation
GIT_COMMIT_ID := $(shell git log --format="%H" -n 1)
IMAGE_ID := $(shell docker images --filter=reference=arnurahman/fww-regulation --format "{{.ID}}")
CONTAINER_ID := $(shell docker ps -aqf "name=fww-regulation")

rebase:
			git fetch && git pull --rebase

prune:
			docker stop $(CONTAINER_ID)
			docker rm $(CONTAINER_ID)
			docker rmi -f $(IMAGE_ID)

build:
			docker build -t arnurahman/${SERVICE_NAME}:latest .

push:
			docker push arnurahman/${SERVICE_NAME}:latest
			docker push arnurahman/${SERVICE_NAME}:$(GIT_COMMIT_ID)

run:
			docker run --env-file /mnt/c/xampp/htdocs/fww-airlines/fww-regulation/service-regulation.env --name ${SERVICE_NAME} --link mariadb -p $(cport):$(cport) -d arnurahman/${SERVICE_NAME}:latest

logs:
			docker logs -f ${SERVICE_NAME}
